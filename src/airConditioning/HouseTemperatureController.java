package airConditioning;

public class HouseTemperatureController {

    private House house;

    public HouseTemperatureController(House house) {
        this.house = house;
    }

    public void checkTemperature(){
        for (Room room: house.getRooms()) {
            if(room.getActualTemperature() > room.getOptimalTemperature()) {
                house.coolRooms();
                //sleepOneSecond();
                //System.out.println(room.getName() + " " + room.getActualTemperature());
            }
        }
    }

    public void sleepOneSecond() {
        try {
            Thread.sleep(1000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }
}
