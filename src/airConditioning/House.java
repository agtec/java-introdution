package airConditioning;

import java.util.ArrayList;
import java.util.List;

public class House {

    private List<Room> rooms;

    public House(List<Room> rooms) {
        this.rooms = rooms;
    }

    public List<Room> getRooms() {
        return rooms;
    }

    public void setRooms(List<Room> rooms) {
        this.rooms = rooms;
    }

    public void coolRooms() {
        for (Room room : rooms) {
            room.coolRoom();
        }
    }

    public boolean areAllRoomCooled() {
        for (Room room : rooms) {
            if (room.getActualTemperature() > room.getOptimalTemperature()) {
                return false;
            }
        }
        return true;
    }
}
