package airConditioning;

public class BasicAirConditioner implements AirConditioner {

    private static final double COOL_TEMPERATURE = 1;

    @Override
    public double lowerTemperature(double temperature, double cubature) {
        temperature = COOL_TEMPERATURE / cubature;
        return temperature;
    }
}
