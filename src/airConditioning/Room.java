package airConditioning;

public class Room {

    private String name;
    private double cubature;
    private boolean isAirConditioningOn = true;
    private double optimalTemperature = 20;
    private double actualTemperature = 25;
    AirConditioner airConditioner;

    public Room(String name, double cubature, double optimalTemperature, double actualTemperature, AirConditioner airConditioner) {
        this.name = name;
        this.cubature = cubature;
        this.optimalTemperature = optimalTemperature;
        this.actualTemperature = actualTemperature;
        this.airConditioner = airConditioner;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public double getCubature() {
        return cubature;
    }

    public void setCubature(double cubature) {
        this.cubature = cubature;
    }

    public boolean isAirConditioningOn() {
        return isAirConditioningOn;
    }

    public void setAirConditioningOn(boolean airConditioningOn) {
        isAirConditioningOn = airConditioningOn;
    }

    public double getOptimalTemperature() {
        return optimalTemperature;
    }

    public void setOptimalTemperature(double optimalTemperature) {
        this.optimalTemperature = optimalTemperature;
    }

    public double getActualTemperature() {
        return actualTemperature;
    }

    public void setActualTemperature(double actualTemperature) {
        this.actualTemperature = actualTemperature;
    }

    public AirConditioner getAirConditioner() {
        return airConditioner;
    }

    public void setAirConditioner(AirConditioner airConditioner) {
        this.airConditioner = airConditioner;
    }


    public void coolRoom() {
        if (actualTemperature > optimalTemperature) {
            actualTemperature = actualTemperature - airConditioner.lowerTemperature(actualTemperature, cubature);
            System.out.printf("%s temperature: %.2f %n", getName(), getActualTemperature());
        }
    }

    @Override
    public String toString() {
        return "room name: " + getName()
                + ", actual temperature: " + getActualTemperature();
    }


}
