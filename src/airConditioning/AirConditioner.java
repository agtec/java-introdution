package airConditioning;

public interface AirConditioner {

    public double lowerTemperature(double temperature, double cubature);

}
