package airConditioning;

import java.util.ArrayList;
import java.util.List;

public class Main {

    public static void main(String[] args) {

        BasicAirConditioner bsa = new BasicAirConditioner();

        ProAirConditioner prc = new ProAirConditioner();

        Room room1 = new Room("dining room", 10, 25, 25, bsa);
        Room room2 = new Room("kitchen", 15, 20, 27, prc);
        Room room3 = new Room("bathroom", 15, 25, 32, prc);

        List<Room> rooms = new ArrayList<>();
        rooms.add(room1);
        rooms.add(room2);
        rooms.add(room3);

        House house = new House(rooms);

        HouseTemperatureController houseTemperatureController = new HouseTemperatureController(house);

        while (house.areAllRoomCooled() == false) {
            houseTemperatureController.checkTemperature();
            houseTemperatureController.sleepOneSecond();
        }






    }
}
