package airConditioning;

public class ProAirConditioner implements AirConditioner {

    public static final double COOL_TEMPERATURE = 2;

    @Override
    public double lowerTemperature(double temperature, double cubature) {
        temperature = COOL_TEMPERATURE / cubature;
        return temperature;
    }
}
