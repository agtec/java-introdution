package paymentCalculator;

public class Main {

    public static void main(String[] args) {

        Employee employee1 = new Employee("Jan", "Kowalski", 3000);
        Employee employee2 = new Employee("Malgorzata", "Nowak", 200);

        PaymentCalculator paymentCalculator = new PaymentCalculator();

        System.out.println(paymentCalculator.nettoYearPayment(employee1));
        System.out.println(paymentCalculator.bruttoYearPayment(employee1));

        System.out.println(paymentCalculator.nettoYearPayment(employee2));
        System.out.println(paymentCalculator.bruttoYearPayment(employee2));
    }
}
