package paymentCalculator;

public class PaymentCalculator {

    private int zus = 1000;
    private double incomeTax = 0.2;

    public double nettoYearPayment(Employee employee) {
        return employee.getMonthlySalary() * 12;
    }

    public double bruttoYearPayment(Employee employee) {
        return (12 * zus + (12 * (employee.getMonthlySalary() / (1 - incomeTax))));
    }
}
