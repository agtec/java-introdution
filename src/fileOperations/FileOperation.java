package fileOperations;

import java.io.*;

public class FileOperation {

    /**
     * counts chars and words from file
     * print the number of chars and words
     */
    public void countCharsAndWords() throws IOException {

        BufferedReader in = null;
        int wordsNumber = 0;
        int charsNumber = 0;

        try {
            in = new BufferedReader(new FileReader("words.csv"));

            String line;

            while ((line = in.readLine()) != null) {
                String[] words = line.split(" ");
                charsNumber += line.length();
                for (String word : words) {
                    if (word.length() > 2) {
                        wordsNumber++;
                    }
                }
            }

        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            if (in != null) {
                in.close();
            }
        }
        System.out.println("Number of characters: " + charsNumber);
        System.out.println("Number of words: " + wordsNumber);
    }

    /**
     * searches for words in the lines of the file and copies these lines to another file
     * @param fileInName
     * @param fileOutName
     * @param word
     * @throws IOException
     */

    public void find(String fileInName, String fileOutName, String word) throws IOException {

        BufferedReader in = null;
        PrintWriter out = null;

        try {
            in = new BufferedReader(new FileReader(fileInName));
            out = new PrintWriter(new FileWriter(fileOutName));

            String line;

            while ((line = in.readLine()) != null) {
                if (line.contains(word)) {
                    out.write(line + "\n");
                }
            }

        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            if (in != null) {
                in.close();
            }

            if (out != null) {
                out.close();
            }

        }
    }
}