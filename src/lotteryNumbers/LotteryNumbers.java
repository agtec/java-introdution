package lotteryNumbers;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.*;

class NumberCounter implements Comparable {
    private int number;
    private int counter;

    public NumberCounter(int number, int counter) {
        this.number = number;
        this.counter = counter;
    }

    public int getNumber() {
        return number;
    }

    public int getCounter() {
        return counter;
    }

    @Override
    public int compareTo(Object o) {
        return ((NumberCounter) o).counter - counter;
    }

    @Override
    public String toString() {
        return number + ": " + counter;
    }
}

public class LotteryNumbers {

    public void downloadData(String filename) throws IOException {

        BufferedReader reader = new BufferedReader(new FileReader(filename));

        List<Integer> winningNumbers = new ArrayList<>();
        List<Integer> megaBall = new ArrayList<>();

        reader.readLine();

        while (reader.readLine() != null) {
            String line = reader.readLine();
            String[] data = line.split(",");
            String[] numbers = data[1].split(" ");
            for (int i = 0; i < numbers.length; i++) {
                winningNumbers.add(Integer.parseInt(numbers[i]));
            }
            if (data.length == 4) {
                megaBall.add(Integer.parseInt(data[data.length - 2]));
            } else {
                megaBall.add(Integer.parseInt(data[data.length - 1]));
            }
        }

        reader.close();
        //System.out.println("winning numbers " + winningNumbers);
        //System.out.println("mega ball " + megaBall);


        TreeMap<Integer, Integer> winnersMap = new TreeMap<Integer, Integer>();
        List<NumberCounter> numberCounters = new ArrayList<>();

        for (int i = 0; i < winningNumbers.size(); i++) {
            if (winnersMap.containsKey(winningNumbers.get(i))) {
                int val = winnersMap.get(winningNumbers.get(i));
                winnersMap.put(winningNumbers.get(i), val + 1);
            } else {
                winnersMap.put(winningNumbers.get(i), 1);
            }
        }

        for (int key : winnersMap.keySet()) {
            int value = winnersMap.get(key);

            numberCounters.add(new NumberCounter(key, value));
        }

        Collections.sort(numberCounters);

        System.out.println("Winner number frequencies: ");

        for (NumberCounter nc : numberCounters) {
            System.out.println(nc);
        }

        TreeMap<Integer, Integer> megaballMap = new TreeMap<Integer, Integer>();
        List<NumberCounter> megaballCounter = new ArrayList<>();

        for (int i = 0; i < megaBall.size(); i++) {
            if (megaballMap.containsKey(megaBall.get(i))) {
                int val = megaballMap.get(megaBall.get(i));
                megaballMap.put(megaBall.get(i), val + 1);
            } else {
                megaballMap.put(megaBall.get(i), 1);
            }
        }

        for (int key : megaballMap.keySet()) {
            int value = megaballMap.get(key);

            megaballCounter.add(new NumberCounter(key, value));
        }

        Collections.sort(megaballCounter);

        System.out.println("Mega ball frequencies: ");

        for (NumberCounter nc : megaballCounter) {
            System.out.println(nc);
        }
    }

    public static void main(String[] args) throws IOException {

        LotteryNumbers lt = new LotteryNumbers();

        lt.downloadData("Lottery.csv");
    }
}
