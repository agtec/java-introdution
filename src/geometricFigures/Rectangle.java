package geometricFigures;

public class Rectangle {

    Point x1;
    Point x2;
    Point x3;
    Point x4;

    public Rectangle(Point x1, Point x2, Point x3, Point x4) {
        this.x1 = x1;
        this.x2 = x2;
        this.x3 = x3;
        this.x4 = x4;
    }


    public Point getX1() {
        return x1;
    }

    public void setX1(Point x1) {
        this.x1 = x1;
    }

    public Point getX2() {
        return x2;
    }

    public void setX2(Point x2) {
        this.x2 = x2;
    }

    public Point getX3() {
        return x3;
    }

    public void setX3(Point x3) {
        this.x3 = x3;
    }

    public Point getX4() {
        return x4;
    }

    public void setX4(Point x4) {
        this.x4 = x4;
    }


    public double areaOfRectangle() {

        Point[] points = {getX1(), getX2(), getX3(), getX4()};

        int a = 0;
        int b = 0;

        for (int i = 0; i < points.length; i++) {
            for (int j = i + 1; j < points.length; j++) {
                if (points[i].getX() == points[j].getX()) {
                    a = (points[i].getY() - points[j].getY());
                    break;
                }
                if (points[i].getY() == points[j].getY()) {
                    b = (points[i].getX() - points[j].getX());
                    break;
                }
            }
        }

        int area = a * b;

        return Math.abs(area);
    }

}
