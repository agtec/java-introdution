package geometricFigures;

public class Calculator {

    public static void main(String[] args) {

        Point p1 = new Point(1, 3);
        Point p2 = new Point(5, 1);
        Point p3 = new Point(5, 3);
        Point p4 = new Point(1, 1);

        Rectangle rectangle = new Rectangle(p1, p2, p3, p4);

        System.out.println(rectangle.areaOfRectangle());

        Circle circle = new Circle(p3, 3);

        System.out.println(p2.isPointInsideCircle(circle));

        Point p5 = new Point(0, 1);

        System.out.println(p5.isPointInsideCircle(circle));
    }
}
