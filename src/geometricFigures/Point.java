package geometricFigures;

public class Point {

    private int x;
    private int y;


    public Point(int x, int y) {
        this.x = x;
        this.y = y;
    }

    public int getX() {
        return x;
    }

    public void setX(int x) {
        this.x = x;
    }

    public int getY() {
        return y;
    }

    public void setY(int y) {
        this.y = y;
    }

    public boolean isPointInsideCircle(Circle circle) {
        if (Math.pow((getX() - circle.getCenter().getX()), 2) + (Math.pow(getY() - circle.getCenter().getY(), 2)) < Math.pow(circle.getRadius(), 2)) {
            return true;
        }
        return false;
    }
}
