import java.util.Scanner;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class EmailRegex {

    // metoda do pobrania danych od użytkownika i wywołania metody verify email aby sprawdzić czy email jest prawidłowy

    public void verifyEmails() {
        Scanner sc = new Scanner(System.in);
        int numberOfEmailAdresses;
        System.out.println("Write the number of email to check and their addresses");
        numberOfEmailAdresses = sc.nextInt();
        sc.nextLine();
        System.out.println("Write email adress: ");
        String[] emailAdresses = new String[numberOfEmailAdresses];
        for (int i = 0; i < numberOfEmailAdresses; i++) {
            String email = sc.nextLine();
            emailAdresses[i] = email;
        }

        for (String email : emailAdresses) {
            System.out.println(verifyEmail(email));
        }
    }

    // metoda do weryfikacji adresu email z zastosowaniem wyrażeń regularnych


    private boolean verifyEmail(String email) {
        // wersja bazowa
        //String EMAIL_REGEX1 = "([a-zA-Z0-9\\.])+@([a-zA-Z0-9\\.])+\\.[a-zA-Z0-9]{2,3}";

        // tutaj dajemy ze 1 czesc ma miec 1-20 znakow
        //String EMAIL_REGEX2 = "([a-zA-Z0-9\\.]){1,20}@([a-zA-Z0-9\\.])+\\.[a-zA-Z0-9]{2,3}";

        // dodajemy zabronione 2 kropki kolo siebie
        //String EMAIL_REGEX3 = "((?!^\\.)[a-zA-Z0-9\\.](?!\\.\\.)){1,20}@([a-zA-Z0-9\\.])+\\.[a-zA-Z0-9]{2,3}";

        // String EMAIL_REGEX4 = "(((?!^\\.)[a-zA-Z0-9\\._](?!\\.\\.))(?<!\\.)){1,20}@(((?!^\\.)[a-zA-Z0-9\\._](?!\\.\\.))){1,20}\\.[a-zA-Z]{2,3}";

        String EMAIL_REGEX5 = "^((?!\\.)([a-zA-Z0-9_\\.](?!\\.\\.)){0,19}[a-zA-Z0-9_]@(?!\\.)([a-zA-Z0-9_\\.](?!\\.\\.)){0,19}[a-zA-Z0-9_]\\.[a-zA-Z]{2,3}$)";

        Pattern p = Pattern.compile(EMAIL_REGEX5);
        Matcher m = p.matcher(email);

        return m.matches();
    }

//    private static void expect(boolean expectedValue, boolean actualValue, String msg) {
//        if (expectedValue == actualValue) {
//            System.out.println("is ok  " + msg);
//        } else {
//            System.out.println("not ok " + msg);
//        }
//    }

    public static void main(String[] args) {

        // tworzę obiekt i sprawdzam funckje dla różnych danych

        EmailRegex emailChecker = new EmailRegex();

//        expect(true, emailChecker.verifyEmail("abc@example.com"), "abc@example.com");
//
//        // 20 znakow w 1 czesci
//        expect(true, emailChecker.verifyEmail("12345678901234567890@example.pl"), "12345678901234567890@example.pl");
//
//        // 21 znakow w 1 czesci
//        expect(false, emailChecker.verifyEmail("123456789012345678901@example.com"), "123456789012345678901@example.pl");
//
//        expect(false, emailChecker.verifyEmail("abc.@example.com"), "abc.@example.com");
//        expect(false, emailChecker.verifyEmail("ab..c@example.com"), "ab..c@example.com");
//        expect(false, emailChecker.verifyEmail(".x@example.com"), ".x@example.com");
//        expect(false, emailChecker.verifyEmail("x@example.x"), "x@example.x");
//        expect(true, emailChecker.verifyEmail("ab_c@exa_mple.com"), "ab_c@exa_mple.com");
//        expect(false, emailChecker.verifyEmail("ab-c@exa-mple.com"), "ab-c@exa-mple.com");

        emailChecker.verifyEmails();
    }
}
