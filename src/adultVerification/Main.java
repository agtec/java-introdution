package adultVerification;

import java.util.ArrayList;
import java.util.List;

public class Main {

    public static void main(String[] args) {

        Person adult1 = new Person("Jan", 20);
        Person adult2 = new Person("Ewa", 18);
        Person child1 = new Person("Mikolaj", 4);
        Person child2 = new Person("Blanka", 15);

        List<Person> personList = new ArrayList();

        personList.add(adult1);
        personList.add(adult2);
        personList.add(child1);
        personList.add(child2);

        for (Person person : personList) {
            System.out.println(person.isAdult(person));
        }
    }
}
