package digitAsAword2;

public class Digit {

    public void digitToWord(int digit, String language) {
        switch (language) {
            case ("pl"):
                printDigitPL(digit, "pl");
                break;
            case ("en"):
                printDigitEN(digit, "en");
                break;
            case ("de"):
                printDigitDE(digit, "de");
                break;
            default:
                System.out.println("Incorrect value, write pl, en or de");
        }
    }

    public void printDigitPL(int digit, String language) {
        for (DigitAsWord d : DigitAsWord.values()) {
            if (digit == d.getValue()) {
                System.out.println(d.PLname);
            }
        }
    }

    public void printDigitEN(int digit, String language) {
        for (DigitAsWord d : DigitAsWord.values()) {
            if (digit == d.getValue()) {
                System.out.println(d.ENname);
            }
        }
    }

    public void printDigitDE(int digit, String language) {
        for (DigitAsWord d : DigitAsWord.values()) {
            if (digit == d.getValue()) {
                System.out.println(d.DEname);
            }
        }
    }
}
