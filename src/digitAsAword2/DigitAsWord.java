package digitAsAword2;

public enum DigitAsWord {
    JEDEN (1, "jeden", "one", "eins"),
    DWA (2, "dwa", "two", "zwei"),
    TRZY (3, "trzy", "three", "drei"),
    CZTERY (4, "cztery", "four", "vier"),
    PIEC (5, "piec", "five", "funf"),
    SZESC (6, "szesc", "six", "sechs"),
    SIEDEM (7, "siedem", "seven", "sieben"),
    OSIEM (8, "osiem", "eight", "acht"),
    DZIEWIEC (9, "dziewiec", "nine", "neun");

    int value;
    String PLname;
    String ENname;
    String DEname;

    DigitAsWord(int value, String PLname, String ENname, String DEname) {
        this.value = value;
        this.PLname = PLname;
        this.ENname = ENname;
        this.DEname = DEname;
    }

    public int getValue() {
        return value;
    }

    public void setValue(int value) {
        this.value = value;
    }

    public String getPLname() {
        return PLname;
    }

    public void setPLname(String PLname) {
        this.PLname = PLname;
    }

    public String getENname() {
        return ENname;
    }

    public void setENname(String ENname) {
        this.ENname = ENname;
    }

    public String getDEname() {
        return DEname;
    }

    public void setDEname(String DEname) {
        this.DEname = DEname;
    }
}
