package sortPlayers;

public class Player implements Comparable<Player> {

    private String nickname;
    private int points;

    public Player(String nickname, int points) {
        this.nickname = nickname;
        this.points = points;
    }

    public String getNickname() {
        return nickname;
    }

    public void setNickname(String nickname) {
        this.nickname = nickname;
    }

    public int getPoints() {
        return points;
    }

    public void setPoints(int points) {
        this.points = points;
    }

    @Override
    public String toString() {
        return "Nickname: " + getNickname() + ", points: " + getPoints();
    }

    @Override
    public int compareTo(Player o) {
        return o.getPoints() - this.points;
    }
}
