package sortPlayers;

import java.util.Arrays;

public class Main {

    public static void main(String[] args) {

        Player[] players = {new Player("Ajaks", 12),
                new Player("Piotr", 22),
                new Player("Xx", 9),
                new Player("Felek", 35),
                new Player("Baks", 33)};

        Arrays.sort(players);
        System.out.println("Sorted by points(descening): " + Arrays.toString(players));

        Arrays.sort(players, new NickNameComparator());
        System.out.println("Sorted by nickname: " + Arrays.toString(players));


    }
}