package exchange;

public class Money implements IMoney {

    protected double amount;

    public Money(double amount) {
        this.amount = amount;
    }

    public double getAmount() {
        return amount;
    }

    @Override
    public double getAmout() {
        return this.amount * 2;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj instanceof Money) {
            if (obj.getClass().equals(this.getClass())) {
                if (((Money) obj).getAmount() == this.getAmount()) {
                    return true;
                }
            }
        }
        return false;
    }
}
