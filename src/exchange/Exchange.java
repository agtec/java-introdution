package exchange;

public class Exchange {

    private double dollarToFrancValue;
    private double francToDollarValue;

    public Exchange(double dollarToFrancValue, double francToDollarValue) {
        this.dollarToFrancValue = dollarToFrancValue;
        this.francToDollarValue = francToDollarValue;
    }

    public double getDollarToFrancValue() {
        return dollarToFrancValue;
    }

    public void setDollarToFrancValue(double dollarToFrancValue) {
        this.dollarToFrancValue = dollarToFrancValue;
    }

    public double getFrancToDollarValue() {
        return francToDollarValue;
    }

    public void setFrancToDollarValue(double francToDollarValue) {
        this.francToDollarValue = francToDollarValue;
    }

    public double dollarToFrancRatio() {
        return getDollarToFrancValue();
    }

    public double francToDollarRatio() {
        return getFrancToDollarValue();
    }

    public Franc dollarToFranc(Dollar dollar) {
        double amount = dollar.getAmount()*1./getDollarToFrancValue();
        return new Franc(amount);
    }

    public Dollar francToDollar(Franc franc) {
        double amount = franc.getAmount()*1./getFrancToDollarValue();
        return new Dollar(amount);
    }
}
