package exchange;

public class Dollar extends Money {

    public Dollar(double amount) {
        super(amount);
    }

    public  double add(Money money) {
        if (money instanceof Dollar) {
            this.amount = this.getAmount() + money.getAmount();
        }

        return this.amount;
    }

    public double add(Money money, Exchange exchange) {
        if (money instanceof Dollar) {
            this.amount = this.getAmount() + money.getAmount();
            return this.getAmount();
        } else if (money instanceof Franc) {
            this.amount = exchange.francToDollar((Franc) money).getAmount() + this.amount;
        }

        return this.amount;
    }
}
