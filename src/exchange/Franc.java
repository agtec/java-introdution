package exchange;

public class Franc extends Money {

    public Franc(double amount) {
        super(amount);
    }

    public double add(Money money) {
        if (money instanceof Franc) {
            this.amount = this.getAmount() + money.getAmount();
        }
        return this.amount;
    }

    public double add(Money money, Exchange exchange) {
        if (money instanceof Franc) {
            this.amount = this.getAmount() + money.getAmount();
            return this.getAmount();
        } else if (money instanceof Dollar) {
            this.amount = exchange.dollarToFranc((Dollar) money).getAmount() + this.amount;
        }
        return this.amount;
    }
}
