package genericTypes;

public class UruchomA {
    public static void main(String[] args) {

        A[] a = {new A<> (1), new A<> ("abc"), new A <> (new B(19))};

        A<Integer> value = new A<> (1);
        A<String> text = new A<> ("abc");
        A<B> objectB = new A <> (new B(10));

        B b = new B(11);
        b.setX(100);
        //System.out.println("get.x: " + value.getX());

        A [] ab = new A[10];
        ab[0] = b;
        ab[1] = new B(80);

        for (A object: a) {
            System.out.println(object.getX());
        }
    }
}
