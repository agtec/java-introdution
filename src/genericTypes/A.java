package genericTypes;

public class A <X> {

    private X x;

    public A(X x) {
        this.x = x;
    }

    public X getX() {
        return x;
    }

    public void setX(X x) {
        this.x = x;
    }
}
