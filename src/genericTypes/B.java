package genericTypes;

public class B extends A<Integer> {

    public B(Integer integer) {
        super(integer);
    }

    @Override
    public Integer getX() {
        return super.getX();
    }

    @Override
    public void setX(Integer integer) {
        super.setX(integer);
    }

    @Override
    public String toString() {
        return "" + getX();
    }
}
