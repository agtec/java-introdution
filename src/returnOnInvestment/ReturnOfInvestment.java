package returnOnInvestment;

public class ReturnOfInvestment {

    public static void main(String[] args) {

        Interval year = Interval.YEAR;

        Investment inw1 = new Investment(1000, 2, year, 3);
        System.out.println(inw1.interestRate(inw1));

        Interval month = Interval.MONTH;

        Investment inw2 = new Investment(2000, 0.05, month, 5);
        System.out.println(inw2.interestRate(inw2));

        Interval day = Interval.DAY;

        Investment inw3 = new Investment(2000, 0.01, day, 3);
        System.out.println(inw3.interestRate(inw3));

        Interval week = Interval.WEEK;

        Investment inw4 = new Investment(2000, 0.03, week, 3);
        System.out.println(inw4.interestRate(inw4));

    }
}
