package returnOnInvestment;

public enum Interval {

    DAY,
    WEEK,
    MONTH,
    YEAR;
}
