package returnOnInvestment;

import java.time.LocalDate;

public class Investment {

    private double investmentValue;
    private double interest;
    private int investmentPeriod;
    Interval interval;
    private int year;

    public Investment(double investmentValue, double interest, Interval interval, int investmentPeriod) {
        this.investmentValue = investmentValue;
        this.interest = interest;
        this.investmentPeriod = investmentPeriod;
        this.interval = interval;
        LocalDate d = LocalDate.now();
        this.year = d.getYear();
    }

    public Investment(double investmentValue, double interest, Interval interval, int investmentPeriod, int year) {
        this.investmentValue = investmentValue;
        this.interest = interest;
        this.investmentPeriod = investmentPeriod;
        this.interval = interval;
        this.year = year;
    }

    public double getInvestmentValue() {
        return investmentValue;
    }

    public void setInvestmentValue(double investmentValue) {
        this.investmentValue = investmentValue;
    }

    public double getInterest() {
        return interest;
    }

    public void setInterest(double interest) {
        this.interest = interest;
    }

    public int getInvestmentPeriod() {
        return investmentPeriod;
    }

    public void setInvestmentPeriod(int investmentPeriod) {
        this.investmentPeriod = investmentPeriod;
    }

    public int getYear() {
        return year;
    }

    public void setYear(int year) {
        this.year = year;
    }

    public double annualInterestRate(Investment investment) {
        double profit = 0;
        double profitFromYears = 0;
        double investmentValue = investment.getInvestmentValue();
        double interestRateOnTheInvestment = investment.getInterest();
        int numberOfDaysInAyear = 0;
        int numberOfWeeksInAyear = 0;

        if (investment.isLeap(investment) == true) {
            numberOfDaysInAyear += 366;
            numberOfWeeksInAyear += 53;
        } else {
            numberOfDaysInAyear += 365;
            numberOfWeeksInAyear += 52;
        }

        if (interval == Interval.DAY) {
            interestRateOnTheInvestment *= numberOfDaysInAyear;
        } else if (interval == Interval.WEEK) {
            interestRateOnTheInvestment *= numberOfWeeksInAyear;
        } else if (interval == Interval.MONTH) {
            interestRateOnTheInvestment *= 12;
        } else if (interval == Interval.YEAR) {
            interestRateOnTheInvestment = this.interest;
        } else {
            System.out.println("Incorrect data");
        }
        return interestRateOnTheInvestment;
    }


    public double interestRate(Investment investment) {
        double profit = 0;
        double profitFromYears = 0;
        double investmentValue = investment.getInvestmentValue();

        for (int i = 0; i < investment.getInvestmentPeriod(); i++) {
            profit = (investmentValue * ((annualInterestRate(investment)) / 100));
            profitFromYears += profit;
            investmentValue += profit;
        }
        double rateOfReturn = ((investmentValue / investment.getInvestmentValue()) - 1) * 100;
        return rateOfReturn;
    }

    private boolean isLeap(Investment investment) {
        boolean is_leap = false;
        if (investment.getYear() % 4 == 0) {
            is_leap = true;
        } else if (investment.getYear() % 100 != 0) {
            is_leap = true;
        } else if (investment.getYear() % 400 != 0) {
            is_leap = false;
        } else {
            is_leap = true;
        }
        return true;
    }
}
