package reversedOrder;

import java.util.Arrays;
import java.util.Scanner;

public class Number {

    public static void main(String[] args) {

        Scanner sc = new Scanner(System.in);

        System.out.println("Enter a string of digits separated by a space: ");
        String digits_str = sc.nextLine();
       // System.out.println(digits_str);

        String[] digits_array = digits_str.split(" ");

        //System.out.println(Arrays.toString(digits_array));

        int index = digits_array.length - 1;

        System.out.println("Digits in the reverse order: ");

        for (int i = 0; i < digits_array.length; i++) {
            System.out.print(digits_array[index - i] + " ");
        }
    }
}
