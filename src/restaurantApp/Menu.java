package restaurantApp;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

public class Menu {

    private List<Dish> menu = new ArrayList<>();

    public Menu() {
        Dish d1 = new Dish(1, "Spaghetti bolonese", 25.50);
        Dish d2 = new Dish(2, "Tagliatelle with prawns", 37.89);
        Dish d3 = new Dish(3, "Penne with meat sauce", 30.00);
        Dish d4 = new Dish(4, "Tomato sup", 17.00);
        Dish d5 = new Dish(5, "Cucumber sup", 18.15);
        Dish d6 = new Dish(6, "Broccoli sup", 16.60);
        Dish d7 = new Dish(7, "Chicken salad", 22.22);
        Dish d8 = new Dish(8, "Vegetable salad", 19.00);
        Dish d9 = new Dish(9, "Apple cake", 10.00);
        Dish d10 = new Dish(10, "Chocolate cake", 11.35);
        Dish d11 = new Dish(11, "Ice cream", 10.80);
        Dish d12 = new Dish(12, "Juice", 7.00);
        Dish d13 = new Dish(13, "Water", 5.00);
        Dish d14 = new Dish(14, "Cola", 7.00);

        menu.addAll(Arrays.asList(d1, d2, d3, d4, d5, d6, d7, d8, d9, d10, d11, d12, d13, d14));
    }

    public List<Dish> getMenu() {
        return menu;
    }

    public void setMenu(List<Dish> menu) {
        this.menu = menu;
    }

    public void printMenu() {
        for (Dish dish : menu) {
            System.out.println(dish);
        }
    }

    Integer strToInt(String intAsStr) {
        try {
            int number = Integer.parseInt(intAsStr.trim());
            return number;
        } catch (NumberFormatException e) {
            System.out.println("Data has been incorrectly entered: " + intAsStr);
        }
        return null;
    }

    public void makeAnOrder(String dishesId) {
        String[] idList = dishesId.split(",");

//        List<Integer> idAsInt = new ArrayList<>();
//        for(String s : idList) {
//            try {
//                int idAsNumber = Integer.parseInt(s.trim());
//                idAsInt.add(idAsNumber);
//            } catch (NumberFormatException e){
//                System.out.println("Data has been incorrectly entered: " + s);
//            }
//        }

        List<Integer> dishesIdAsNumbers = Arrays.stream(idList)
                .map(intAsStr -> strToInt(intAsStr))
                .filter(number -> number != null)
                .collect(Collectors.toList());

        double priceForMenu = 0;

        for (int idAsInt : dishesIdAsNumbers) {
            for (Dish dish : menu) {
                if (dish.getId() == idAsInt) {
                    priceForMenu += dish.getPrice();
                }
            }
        }

        double serviceCharge = serviceCharge(priceForMenu);

        System.out.printf("%s%.2f%n", "The price for order is: ", (priceForMenu + serviceCharge));

        System.out.printf("%s%.2f%n", "Service charge is: ", serviceCharge);

        System.out.printf("%s%.2f%n", "The cost of meals: ", priceForMenu);


    }

    public double serviceCharge(double mealCost) {
        double serviceCharge = 0;
        if (mealCost < 100) {
            serviceCharge = (mealCost * 0.15);
        } else {
            serviceCharge = (mealCost * 0.10);
        }

        return serviceCharge;
    }
}
