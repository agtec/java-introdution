package restaurantApp;

import java.util.Scanner;

public class Main {

    public static void main(String[] args) {

        boolean displayMenu = true;
        Scanner sc = new Scanner(System.in);
        Menu menu = new Menu();

        do {
            System.out.println("\n\n          Restaurant menu");
            System.out.println("--------------'------------------------");
            System.out.println("1 - Show menu");
            System.out.println("2 - Make order");
            System.out.println("3 - Exit");
            System.out.print("\nSelect a Menu Option: ");

            try {
                int input = sc.nextInt();
                sc.nextLine();
                switch (input) {
                    case 1:
                        menu.printMenu();
                        break;
                    case 2:
                        System.out.println("Enter the menu numbers that you want to order, separated by a comma: ");
                        String order = sc.nextLine();
                        menu.makeAnOrder(order);
                        break;
                    case 3:
                        displayMenu = false;
                        break;
                }
            } catch (NumberFormatException n) {
                n.printStackTrace();
            }
        } while (displayMenu);
    }
}
