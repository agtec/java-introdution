package csvFiles;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class Data {

    BufferedReader in = null;

    public List<String> readDataProducts(String filename) throws IOException {

        List<String> products = new ArrayList<>();

        try {

            in = new BufferedReader(new FileReader(filename));

            String s;

            while ((s = in.readLine()) != null) {
                String[] data = s.split(";");
                products.add(data[0]);
            }
        } finally {
            if (in != null) {
                in.close();
            }
        }
        return products;
    }


    public List<Integer> readDataAmount(String filename) throws IOException {

        List<Integer> amount = new ArrayList<>();

        try {

            in = new BufferedReader(new FileReader(filename));

            String s;

            while ((s = in.readLine()) != null) {
                String[] data = s.split(";");
                amount.add(Integer.valueOf(data[1]));
            }
        } finally {
            if (in != null) {
                in.close();
            }
        }
        return amount;
    }


    public void printProductAndAmount(String filename) throws IOException {
        List<String> products = readDataProducts(filename);
        List<Integer> amount = readDataAmount(filename);
        System.out.println(products);
        System.out.println(amount);
    }


    public void percentageGraph(String filename) throws IOException {
        List<String> products = readDataProducts(filename);
        List<Integer> amount = readDataAmount(filename);

        List<Integer> roundAmount = new ArrayList<>();

        int sum = 0;

        // zaokraglam

        for (int i = 0; i < amount.size(); i++) {
            if (amount.get(i) % 10 == 0) {
                roundAmount.add(amount.get(i));
            } else if (amount.get(i) % 10 < 5) {
                int roundAm = amount.get(i) - ((amount.get(i) % 10));
                roundAmount.add(roundAm);
            } else if (amount.get(i) % 10 >= 5) {
                int roundAm = amount.get(i) + (10 - (amount.get(i) % 10));
                roundAmount.add(roundAm);
            }
        }
        for (int i = 0; i < roundAmount.size(); i++) {
            sum += roundAmount.get(i);
        }

        String star = "";

        for (int i = 0; i < roundAmount.size(); i++) {
            System.out.print(products.get(i) + ", " + amount.get(i) + " " + roundAmount.get(i));
            for (int j = 0; j < (roundAmount.get(i) / 10); j++) {
                star += "*";
            }
            System.out.println(" " + star);
            star = "";
        }
    }
}
