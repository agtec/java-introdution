package digitAsAword;

public class Digit {

    public void digitsPl(int digit) {
        switch (digit) {
            case 0:
                System.out.println("zero");
                break;
            case 1:
                System.out.println("jeden");
                break;
            case 2:
                System.out.println("dwa");
                break;
            case 3:
                System.out.println("trzy");
                break;
            case 4:
                System.out.println("cztery");
                break;
            case 5:
                System.out.println("piec");
                break;
            case 6:
                System.out.println("szesc");
                break;
            case 7:
                System.out.println("siedem");
                break;
            case 8:
                System.out.println("osiem");
                break;
            case 9:
                System.out.println("dziewiec");
                break;
            default:
                System.out.println("niewlasciwa wartosc");
        }
    }

    public void digitsEN(int digit) {
        switch (digit) {
            case 0:
                System.out.println("zero");
                break;
            case 1:
                System.out.println("one");
                break;
            case 2:
                System.out.println("two");
                break;
            case 3:
                System.out.println("three");
                break;
            case 4:
                System.out.println("four");
                break;
            case 5:
                System.out.println("five");
                break;
            case 6:
                System.out.println("six");
                break;
            case 7:
                System.out.println("seven");
                break;
            case 8:
                System.out.println("eight");
                break;
            case 9:
                System.out.println("nine");
                break;
            default:
                System.out.println("incorrect value");
        }
    }

    public void printWord(int digit, String language) {
        switch (language) {
            case "pl":
                digitsPl(digit);
                break;
            case "en":
                digitsEN(digit);
                break;
        }
    }
}
