package digitAsAword;

public class Main {

    public static void main(String[] args) {

        Digit digit = new Digit();
        digit.printWord(1, "en");
        digit.printWord(5, "en");
        digit.printWord(1, "pl");
        digit.printWord(9, "pl");
        digit.printWord(10, "pl");
    }
}
