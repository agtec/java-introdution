package stack;

import java.util.Scanner;

public class Main {

    public static void main(String[] args) {

        Stack stack = new Stack();

        Scanner sc = new Scanner(System.in);
        System.out.println("Enter the number of operations: ");
        int numberOfOperations = sc.nextInt();
        String set;
        int number;

        for (int i = 0; i < numberOfOperations; i++) {
            System.out.println("Enter + (push) or - (pop): ");
            set = sc.next();
            if (set.equals("+")) {
                System.out.println("Enter number: ");
                number = sc.nextInt();
                System.out.println(stack.push(number));
            } else if (set.equals("-")) {
                System.out.println(stack.pop());
            }
        }
    }
}
