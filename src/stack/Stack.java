package stack;

public class Stack {

    int[] array = new int[10];
    int currentIndex = 0;


    public String push(int value) {
        if (currentIndex < array.length) {
            array[currentIndex] = value;
            currentIndex++;
            return ":)";
        } else {
            return ":(";
        }
    }

    public String pop() {
        if (currentIndex == 0) {
            return ":(";
        } else {
            String value = "" + (array[currentIndex -1]);
            currentIndex--;
            return value;
        }
    }
}
