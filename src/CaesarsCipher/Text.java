package CaesarsCipher;

public class Text {

    char[] letters = {'A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'K', 'L', 'M', 'N', 'O', 'P', 'Q', 'R', 'S', 'T', 'U', 'V', 'W', 'X', 'Y', 'Z'};

    public String encrypt(String text) {
        if (text.length() > 200) {
            System.out.println("The length of the text exceeds 200 characters");
        }

        char[] textArray = text.toCharArray();
        String cipher = "";

        for (char ch : textArray) {
            for (int i = 0; i < letters.length; i++) {
                if (ch == letters[i]) {
                    cipher += letters[(i + 3) % letters.length];
                } else if (ch == ' ' || ch == '\n') {
                    cipher += ch;
                    break;
                }
            }
        }
        return cipher;
    }

    public String decrypt(String text) {
        if (text.length() > 200) {
            System.out.println("The length of the text exceeds 200 characters");
        }

        char[] textArray = text.toCharArray();
        String cipher = "";

        for (char ch : textArray) {
            for (int i = 0; i < letters.length; i++) {
                if (ch == letters[i]) {
                    cipher += letters[(i - 3) % letters.length];
                } else if (ch == ' ' || ch == '\n') {
                    cipher += ch;
                    break;
                }
            }
        }
        return cipher;
    }

}
