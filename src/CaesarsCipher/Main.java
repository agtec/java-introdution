package CaesarsCipher;

public class Main {

    public static void main(String[] args) {

        Text text = new Text();
        System.out.println(text.encrypt("ABC DEF  GHI"));
        System.out.println(text.decrypt("DEF GHI  JKL"));
    }
}
