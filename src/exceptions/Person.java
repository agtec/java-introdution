package exceptions;

public class Person {

    public static void main(String[] args) {

        String name = null;

        try {
            name.isEmpty();
            name.length();
            name.charAt(10);
        } catch (NullPointerException e) {
            System.out.println("You did not enter the name");
            name = "X";
        } catch (StringIndexOutOfBoundsException diferentProblem) {
            diferentProblem.printStackTrace();
            System.out.println("nie ma mniej niz 10");
        } catch (Throwable a) {
            System.out.println("This is a general error");
        } finally {
            System.out.println("This piece of code will always be done");
        }

        System.out.println("Hello " + name);

        checkAge(19);
        checkAge(10);
    }

    public static void checkAge(int age) {
        if (age < 18) {
            throw new UnderageException(18, "You are underage");
        }
    }
}
