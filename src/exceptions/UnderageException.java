package exceptions;

public class UnderageException extends RuntimeException {

    public UnderageException(int statusCode, String message) {
            super(message);
        }
}
