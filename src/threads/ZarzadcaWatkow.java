package threads;

public class ZarzadcaWatkow {

    public void utworzWatki(int ilosc) {
        Runnable[] watki = new Runnable[ilosc];
        for (int i = 0; i < ilosc; i++) {
            watki[i] = new Watek(watki, i);
        }
        new Thread(watki[0]).start();
    }
}
