package threads;

public class Watek implements Runnable{

    Runnable[] watki;
    int index;

    public Watek(Runnable[] watki, int index) {
        this.watki = watki;
        this.index = index;
    }

    @Override
    public void run() {
        System.out.println("To jest watek nr: " + Thread.currentThread().getName());
        if (index<watki.length-1) {
            new Thread(watki[index+1]).start();
        }

    }
}
