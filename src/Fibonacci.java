import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

/*
 program wypisuje ciąg Fibonacciego do podanego na wejściu elementu ciągu
 */

public class Fibonacci {

    public void printFibonacciNumbers() {
        Scanner sc = new Scanner(System.in);
        int lastNumber = sc.nextInt();
        List<Integer> fibonacciNumbers = calculateFibonacciNumbers(lastNumber);
        for (int element : fibonacciNumbers) {
            System.out.print(element + " ");
        }
    }

    public List<Integer> calculateFibonacciNumbers(int lastNumber) {

        // tworze liste cyfr dla ciagu i dodaje pierwszy el = 0 i dla tego el. wypiszemy 0;

        List<Integer> fibonacciNumbers = new ArrayList<Integer>();

        fibonacciNumbers.add(0);

        // jezeli uzytkownik wpisze liczbe >= 1, to dodaje 1 (dla pierwszego wyrazu ciagu wypiszemy 1)

        if (lastNumber >= 1) {
            fibonacciNumbers.add(1);
        }

        // mam dwie cyfry pod indeksem 0 i 1 wiec jak ktos poda cyfre > 1 to iterujemy od indeksu 2
        // aby przypisac pod ten indeks kolejna cyfre

        for (int i = 2; i < lastNumber; i++) {
            int nextNumber = (fibonacciNumbers.get(i - 1)) + (fibonacciNumbers.get(i - 2));
            if (nextNumber > lastNumber) {
                break;
            }
            fibonacciNumbers.add(nextNumber);
        }
        return fibonacciNumbers;
    }

    public static void main(String[] args) {

        Fibonacci fib = new Fibonacci();
        fib.printFibonacciNumbers();
    }
}
