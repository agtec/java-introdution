package compare;

import java.util.Comparator;

public class NameOfClubComparator implements Comparator<Player> {
    @Override
    public int compare(Player o1, Player o2) {
        return o1.getNameOfClub().compareTo(o2.getNameOfClub());
    }
}
