package compare;

public class Player implements Comparable<Player> {

    private String name;
    private String lastname;
    private int age;
    private int ranking;
    private int shoeSize;
    String nameOfClub;

    public Player(String name, String lastname, int age, int ranking) {
        this.name = name;
        this.lastname = lastname;
        this.age = age;
        this.ranking = ranking;
    }

    public Player(String name, String lastname, int age, int ranking, int shoeSize, String nameOfClub) {
        this.name = name;
        this.lastname = lastname;
        this.age = age;
        this.ranking = ranking;
        this.shoeSize = shoeSize;
        this.nameOfClub = nameOfClub;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getLastname() {
        return lastname;
    }

    public void setLastname(String lastname) {
        this.lastname = lastname;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public int getRanking() {
        return ranking;
    }

    public void setRanking(int ranking) {
        this.ranking = ranking;
    }

    public int getShoeSize() {
        return shoeSize;
    }

    public void setShoeSize(int shoeSize) {
        this.shoeSize = shoeSize;
    }

    public String getNameOfClub() {
        return nameOfClub;
    }

    public void setNameOfClub(String nazwaKlubu) {
        this.nameOfClub = nazwaKlubu;
    }

    @Override
    public int compareTo(Player player) {
        return this.getRanking() - player.getRanking();
    }

    // tu porownujemy Stringi

    public int compareTo2(Player player) {
        return player.getName().compareTo(this.name);
    }

    @Override
    public String toString() {
        return getName() + " " + getLastname() + " " + getAge() + " " + getRanking() + " " + getShoeSize() + " " + getNameOfClub();
    }
}
