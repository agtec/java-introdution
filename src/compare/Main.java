package compare;

import java.util.Collections;
import java.util.Comparator;
import java.util.LinkedList;
import java.util.List;

public class Main {

    public static void main(String[] args) {

        Player pl1 = new Player("abc", "Kowalski", 23, 100, 40, "Klub1");
        Player pl2 = new Player("def", "Nowak", 27, 79, 42, "Klub2");
        Player pl3 = new Player("ghi", "Wozniak", 33, 19, 46, "InnyKlub");
        Player pl4 = new Player("jkl", "Mickiewicz", 20, 55, 38, "MalyKlub");
        Player pl5 = new Player("mno", "Kochanowski", 34, 99, 44, "anonimowyKlub");
        Player pl6 = new Player("prs", "Tuwim", 40, 60, 41, "AnonimowyKlub");

        System.out.println(pl1.compareTo(pl2));
        System.out.println(pl1.compareTo(pl3));

        List<Player> playersList = new LinkedList<Player>();
        playersList.add(pl1);
        playersList.add(pl2);
        playersList.add(pl3);
        playersList.add(pl4);
        playersList.add(pl5);
        playersList.add(pl6);

        System.out.println("Przed sortowaniem: ");
        System.out.println();

        for (Player pl : playersList) {
            System.out.println(pl);
        }
        Collections.sort(playersList, Player::compareTo);

        System.out.println("\nPo sortowaniu: ");

        for (Player pl : playersList) {
            System.out.println(pl);
        }

        System.out.println("\nPorownujemy po wieku: ");

        Collections.sort(playersList, new Comparator<Player>() {
            @Override
            public int compare(Player o1, Player o2) {
                return 0;
            }
        });

        PlayerRankingComparator plc = new PlayerRankingComparator();
        Collections.sort(playersList, plc);

        System.out.println("\nPo sortowaniu z player ranking comparator: ");

        for (Player pl : playersList) {
            System.out.println(pl);
        }

        // komparator wieku

        AgeComparator ac = new AgeComparator();
        Collections.sort(playersList, ac);

        System.out.println("\nPo sortowaniu z age comparator: ");

        for (Player pl : playersList) {
            System.out.println(pl);
        }

        ShoeSizeComparator ssc = new ShoeSizeComparator();
        Collections.sort(playersList, ssc);

        System.out.println("\nPo sortowaniu z shoe size comparator: ");

        for (Player pl : playersList) {
            System.out.println(pl);
        }

        NameOfClubComparator noc = new NameOfClubComparator();
        Collections.sort(playersList, noc);

        System.out.println("\nPo sortowaniu z name of club comparator: ");

        for (Player pl : playersList) {
            System.out.println(pl);
        }

        System.out.println(pl1.compareTo2(pl2));
    }
}
