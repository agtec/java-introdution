package compare;

import java.util.Comparator;

public class ShoeSizeComparator implements Comparator<Player> {
    @Override
    public int compare(Player o1, Player o2) {
        return o1.getShoeSize() - o2.getShoeSize();
    }
}
