package student;

public class Main {

    public static void main(String[] args) {

        Student s1 = new Student("X", "ABCD");
        Student s2 = new Student("Y", "EFG");

        // dodajemy przedmiot i oceny

        // dodajemy przedmiot do listy studenta

        s1.addSubjectForStudent(Activity.BIOLOGY);
        s1.addSubjectForStudent(Activity.CHEMISTRY);
        s1.addSubjectForStudent(Activity.CHEMISTRY_LABS);
        s1.addSubjectForStudent(Activity.MATH);


        s2.addSubjectForStudent(Activity.BIOLOGY);
        s2.addSubjectForStudent(Activity.MATH);

        s1.addGrade(Activity.BIOLOGY, null);
        s1.addGrade(Activity.CHEMISTRY, null);
        s1.addGrade(Activity.CHEMISTRY_LABS, 5);
        s1.addGrade(Activity.MATH, 6);

        s2.addGrade(Activity.MATH, 3);
        s2.addGrade(Activity.BIOLOGY, 5);


        // usuwa przedmiot

        s1.removeSubjectForStudent(Activity.BIOLOGY);

        // pokazuje przedmioty

        s1.showStudentSubjects();
        s2.showStudentSubjects();

        // pokazuje srednia

        System.out.println(s1.getAverageOfStudentGrades());
        System.out.println(s2.getAverageOfStudentGrades());

        System.out.println(s1.subjectsWithoutGrade());
        System.out.println(s1.subjectWithoutGrade2());

        // porownuje student ID

        System.out.println("Id student1: " + s1.getStudentID());
        System.out.println("Id student2: " + s2.getStudentID());

        StudentIDComparator studentIDComparator = new StudentIDComparator();
        System.out.println(studentIDComparator.compare(s1, s2));

        // porownuje average of student grades

        System.out.println("The average of student1 grades: " + s1.getAverageOfStudentGrades());
        System.out.println("The average of student1 grades: " + s2.getAverageOfStudentGrades());

        AverageOfStudentGradesComparator averageOfStudentGradesComparator = new AverageOfStudentGradesComparator();
        System.out.println(averageOfStudentGradesComparator.compare(s1, s2));

    }
}
