package student;

import java.util.*;

public class Student {

    private String name;
    private String lastname;
    private Integer studentID;
    private List<Activity> listOfStudentSubjects;
    private Map<Activity, Integer> studentGrades;
    private Random random = new Random();

    public Student(String name, String lastname) {
        this.name = name;
        this.lastname = lastname;
        this.studentID = random.nextInt(((99999 - 10000) + 1) - 10000);
        //random = ((max - min)+1)-max

        this.listOfStudentSubjects = new ArrayList<>();
        this.studentGrades = new HashMap<>();
    }


    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getLastname() {
        return lastname;
    }

    public void setLastname(String lastname) {
        this.lastname = lastname;
    }

    public Integer getStudentID() {
        return studentID;
    }

    public void setStudentID(Integer studentID) {
        this.studentID = studentID;
    }

    public List<Activity> getListOfStudentSubjects() {
        return listOfStudentSubjects;
    }

    public void setListOfStudentSubjects(List<Activity> listOfStudentSubjects) {
        this.listOfStudentSubjects = listOfStudentSubjects;
    }

    public Map<Activity, Integer> getStudentGrades() {
        return studentGrades;
    }

    public void setStudentGrades(Map<Activity, Integer> studentGrades) {
        this.studentGrades = studentGrades;
    }

    public String addGrade(Activity activity, Integer subjectGrade) {
        // spr czy student uczeszczal na dany przedmiot
        // przypisanie oceny do przedmiotu - do mapy

        for (Activity subject : getListOfStudentSubjects()) {
            if (subject.equals(activity)) {
                getStudentGrades().put(activity, subjectGrade);
            } else {
                System.out.println("This subject is not in the list");
            }
        }
        return "The subject grade added to " + activity;
    }

    public void addSubjectForStudent(Activity subject) {
        //dodajemy do listy przedmiotow

        getListOfStudentSubjects().add(subject);
        System.out.println("Added: " + subject);
    }


    public void removeSubjectForStudent(Activity subject) {
        getListOfStudentSubjects().remove(subject);
        System.out.println("Removed: " + subject);
    }

    public void showStudentSubjects() {
        for (Activity activity : getListOfStudentSubjects()) {
            System.out.print(activity + " ");
        }
        System.out.println();
    }

    public Double getAverageOfStudentGrades() {
        // zsumowac z mapy - zamienic ja na set
        // iterowac i wyciagac oceny, obliczyc srednia
        Set<Integer> set = getSetFromMapOfGrades();
        int licznik = 0;
        double sum = 0;
        for (Integer i : set) {
            if (i != null) {
                sum += i.doubleValue();
                licznik++;
            }

        }
        double average = sum / licznik;
        return average;
    }

    public List<Activity> subjectsWithoutGrade() {
        //iterujemy po mapie, spr gdzie jest null
        // zwracamy liste przedmiotow z wartosciami null
        // usunac przedmiot tez z listy studentGrades jak sie usuwa z listofSubject

        List<Activity> subjWithoutGrade = new ArrayList<>();

        for (Map.Entry<Activity, Integer> entry : getStudentGrades().entrySet()) {
            if (entry.getValue() == null) {
                subjWithoutGrade.add(entry.getKey());
            }
        }

        // iterujemy po kluczu i wartosci - mapa
       /*
       for ( Map.Entry<String, Tab> entry : hash.entrySet()) {
    String key = entry.getKey();
    Tab tab = entry.getValue();
    // do something with key and/or tab
}
        */

        return subjWithoutGrade;
    }

    /*
    for (int id: osoby.keySet()) {
            String value = osoby.get(id).getImie().toString() + " " + osoby.get(id).getNazwisko().toString();
            System.out.println(id + " " + value);
        }
     */

    public List<Activity> subjectWithoutGrade2() {
        List<Activity> subjWithoutGrade = new ArrayList<>();

        for (Activity activity : getListOfStudentSubjects()) {
            for (Activity key : getStudentGrades().keySet()) {
                if (activity == key && getStudentGrades().get(key) == null) {
                    subjWithoutGrade.add(activity);
                }
            }
        }

        return subjWithoutGrade;
    }

    public Set<Integer> getSetFromMapOfGrades() {
        // zamiana mapy na zbior integer
        //Set setGrades = studentGrades.entrySet();

        Set<Integer> setGrades = new HashSet<Integer>(getStudentGrades().values());

        return setGrades;
    }

    @Override
    public String toString() {
        return getName() + " " + getLastname() + " " + getStudentID();
    }

    /*Random r = new Random();

    int getAlbumNo = r.nextInt(((10 - 5) + 1) + 5);

    public static void main(String[] args) {
        Random r = new Random();

        int x = r.nextInt(((10 - 5) + 1) + 5);
        System.out.println(x);*/

        /*int y = r.nextInt((1)*10);
        System.out.println(y);*/
}
