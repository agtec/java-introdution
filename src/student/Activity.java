package student;

public enum Activity {
    MATH,
    CHEMISTRY,
    CHEMISTRY_LABS,
    BIOLOGY;
}
