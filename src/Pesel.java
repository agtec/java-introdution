import java.util.Scanner;

public class Pesel {

    public void sprawdzCzyPoprawny() {
        Scanner sc = new Scanner(System.in);
        int ileDanych = sc.nextInt();
        sc.nextLine();
        String[] tablicaPesel = new String[ileDanych];
        for (int i = 0; i < ileDanych; i++) {
            String pesel = sc.nextLine();
            tablicaPesel[i] = pesel;
        }
        for (String pesel : tablicaPesel) {
            if (czyPoprawny(pesel) == true) {
                System.out.println("D");
            } else {
                System.out.println("N");
            }
        }
    }

    private boolean czyPoprawny(String pesel) {

        Integer[] mnozniki = {1, 3, 7, 9, 1, 3, 7, 9, 1, 3, 1};
        int suma = 0;

        if (pesel.length() != 11) {
            return false;
        } else {
            for (int i = 0; i < pesel.length(); i++) {
                char znak = pesel.charAt(i);
                suma += (Integer.parseInt(String.valueOf(znak)) * mnozniki[i]);
            }
            if (suma % 10 == 0) {
                return true;
            }
        }

        return false;
    }

    public static void main(String[] args) {

        Pesel pesel = new Pesel();
        pesel.sprawdzCzyPoprawny();
    }
}
