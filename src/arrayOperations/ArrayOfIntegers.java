package arrayOperations;

import java.util.Arrays;
import java.util.Random;

public class ArrayOfIntegers {

    int size;
    int[] array;

    public ArrayOfIntegers(int size) {
        this.size = size;
        this.array = new int[size];
    }

    // wypelniam liczbami calkowitymi

    public int[] fill() {
        for (int i = 0; i < this.size; i++) {
            this.array[i] = i + 1;
        }
        return this.array;
    }

    public int[] fillWithRandomValues(int range) {
        Random generator = new Random();
        for (int i = 0; i < this.size; i++) {
            this.array[i] = (generator.nextInt(range));
        }
        return this.array;

    }

    // wypisuje w kolejnosci od pocztaku

    public void print() {
        System.out.println(Arrays.toString(this.array));
    }

    // wypisuje od konca

    public void printReversed() {
        int index = this.array.length - 1;
        System.out.print("[");
        while (index >= 0) {
            System.out.print(this.array[index]);
            if (index > 0) {
                System.out.print(", ");
            }
            index--;
        }
        System.out.println("]");
    }

    public void printOdd() {
        System.out.println("Odd numbers: ");
        for (int i : this.array) {
            if (i % 2 != 0) {
                System.out.print(i + " ");
            }
        }
        System.out.println();
    }

    public void printNumbersThatDivideBy3() {
        System.out.print("Numbers divided by three: ");
        for (int i : this.array) {
            if (i % 3 == 0) {
                System.out.print(i + " ");
            }
        }
        System.out.println();
    }

    public void printSumOfAll() {
        int sum = 0;
        for (int i : this.array) {
            sum += i;
        }
        System.out.println("The sum if all numbers: " + sum);
    }

    public void printSumOfFirstFour() {
        int sum = 0;
        for (int i = 0; i < 4; i++) {
            sum += this.array[i];
        }
        System.out.println("The sum of first four numbers: " + sum);
    }

    public void printSumOfLastFiveNumbersGreaterThan2() {
        int index = this.array.length - 1;
        int sum = 0;
        int iCounter = 0;
        for (int i = 0; i < this.array.length; i++) {
            if (this.array[index - i] > 2) {
                sum += this.array[index - i];
                iCounter++;
                if (iCounter == 5) {
                    break;
                }
            }
        }
        System.out.println("The sum of last five numbers greater than two: " + sum);
    }

    public void printSumOfFirstFiveNumbersGreaterThan10() {
        int sum = 0;
        for (int i : this.array) {
            sum += i;
            if (sum > 10) {
                break;
            }
        }
        System.out.println("The sum of first five numbers, that gives sum of ten: " + sum);
    }
}
