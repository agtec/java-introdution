package arrayOperations;

public class Main {

    public static void main(String[] args) {

        System.out.println("Methods for the table with increasing values: ");

        ArrayOfIntegers array = new ArrayOfIntegers(10);
        array.fill();
        array.print();
        array.printReversed();
        array.printOdd();
        array.printNumbersThatDivideBy3();
        array.printSumOfAll();
        array.printSumOfFirstFour();
        array.printSumOfLastFiveNumbersGreaterThan2();
        array.printSumOfFirstFiveNumbersGreaterThan10();

        System.out.println("Method for the table with random values: ");

        ArrayOfIntegers randomArray = new ArrayOfIntegers(10);
        randomArray.fill();
        randomArray.print();
        randomArray.printReversed();
        randomArray.printOdd();
        randomArray.printNumbersThatDivideBy3();
        randomArray.printSumOfAll();
        randomArray.printSumOfFirstFour();
        randomArray.printSumOfLastFiveNumbersGreaterThan2();
        randomArray.printSumOfFirstFiveNumbersGreaterThan10();

    }
}
