package bar;

public class Main {

    public static void main(String[] args) {

        Barman barman = new Barman();

        Drink drink1 = barman.createDrink("water", 50, "alcohol", 40, "orange juice", 20);
        Drink drink2 = barman.createDrink("cola", 30, "alcohol", 50, "ice", 20);

        barman.printDrink(drink1);
        barman.printDrink(drink2);
    }
}
