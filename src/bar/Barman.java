package bar;

public class Barman {

    public Drink createDrink (String name1, double amount1, String name2, double amount2, String name3, double amount3) {
        Drink drink = new Drink();
        Ingredient ingredient1 = new Ingredient(name1, amount1);
        Ingredient ingredient2 = new Ingredient(name2, amount2);
        Ingredient ingredient3 = new Ingredient(name3, amount3);
        drink.ingredients = new Ingredient[]{ingredient1, ingredient2, ingredient3};
        return drink;
    }

    public void printDrink(Drink drink) {
        double overalCapacity = 0;
        for (Ingredient ingredient:drink.ingredients) {
            overalCapacity += ingredient.getAmount();
        }
        double ratioOfIngredient1 = drink.ingredients[0].getAmount()/overalCapacity;
        double ratioOfIngredient2 = drink.ingredients[1].getAmount()/overalCapacity;
        double ratioOfIngredient3 = drink.ingredients[2].getAmount()/overalCapacity;

        System.out.println("The ingredients of drink are: " + drink.ingredients[0].getName() + ", " + drink.ingredients[1].getName() + ", "
                + drink.ingredients[2].getName() + ", in proportions: ");
        System.out.format("%.2f ", ratioOfIngredient1);
        System.out.format("%.2f ", ratioOfIngredient2);
        System.out.format("%.2f", ratioOfIngredient3);
        System.out.println(", its capacity is " + overalCapacity);
    }

    // System.out.format("%.2f%n", a)
}
