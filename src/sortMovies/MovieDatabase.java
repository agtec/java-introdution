package sortMovies;

import java.util.*;

public class MovieDatabase {
    public static void main(String[] args) {
        List<Movie> moviesList = createMoviesList();
        Comparator<Movie> sortAlgorithm = getSortAlgorithm();
        moviesList.sort(sortAlgorithm);
        print(moviesList);
    }

    private static List<Movie> createMoviesList() {
        List<Movie> movies = new ArrayList<>();
        movies.add(new Movie("Shrek", "Andrew Adamson, Vicky Jenson", 2001, 7.75));
        movies.add(new Movie("GoldenEye", "Johnston Joe", 1995, 7.1));
        movies.add(new Movie("A Fish Called Wanda", "John Cleese, Charles Crichton", 1988, 7.3));
        movies.add(new Movie("Pretty Woman", "Gary Marshall", 1990, 7.4));
        movies.add(new Movie("Yogi Bear", "Eric Brevig", 2010, 5.8));
        movies.add(new Movie("Avengers: Infinity War", "Anthony Russo, Joe Russo", 2018, 8.2));
        movies.add(new Movie("Rocky2", "Sylwester Stallone", 1979, 7.5));
        return movies;
    }

    private static Comparator<Movie> getSortAlgorithm() {
        System.out.println("Enter the criterion after which you want to sort (Title/Rating/Year): ");
        Scanner sc = new Scanner(System.in);
        String criteria = sc.nextLine();
        criteria = criteria.toLowerCase();
        for (ComparisionCriteria cc : ComparisionCriteria.values()) {
            if (cc.getCriteria().equals(criteria)) {
                return cc.getMovieComparator();
            }
        }
        return null;
    }

    private static void print(List<Movie> movies) {
        System.out.println("Lista filmów:");
        movies.forEach(System.out::println);
    }
}
