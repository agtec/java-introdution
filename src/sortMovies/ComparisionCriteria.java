package sortMovies;

import java.util.Comparator;

public enum ComparisionCriteria {

    TITLE("title", new Comparator<Movie>() {
        @Override
        public int compare(Movie o1, Movie o2) {
            return o1.getTitle().compareTo(o2.getTitle());
        }
    }),
    RATING("rating", new Comparator<Movie>() {
        @Override
        public int compare(Movie o1, Movie o2) {
            int result = (int) ((o1.getRating() * 10) - (o2.getRating() * 10));
            return result;
        }
    }),
    YEAR("year", new Comparator<Movie>() {
        @Override
        public int compare(Movie o1, Movie o2) {
            return o1.getYear() - o2.getYear();
        }
    });

    private String criteria;
    private Comparator<Movie> movieComparator;

    ComparisionCriteria(String criteria, Comparator<Movie> movieComparator) {
        this.criteria = criteria;
        this.movieComparator = movieComparator;
    }

    public String getCriteria() {
        return criteria;
    }

    public void setCriteria(String criteria) {
        this.criteria = criteria;
    }

    public Comparator<Movie> getMovieComparator() {
        return movieComparator;
    }

    public void setMovieComparator(Comparator<Movie> movieComparator) {
        this.movieComparator = movieComparator;
    }
}
