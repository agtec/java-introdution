package retiring;

import java.io.*;

public class Retiring {

    private static final int WOMAN_AGE = 60;
    private static final int MAN_AGE = 65;

    public void retiring(String fileName) throws IOException {

        BufferedReader in = null;
        PrintWriter outW = null;
        PrintWriter outM = null;
        String line;

        try {

            in = new BufferedReader(new FileReader(fileName));
            outW = new PrintWriter(new FileWriter("women.csv"));
            outM = new PrintWriter(new FileWriter("men.csv"));

            while ((line = in.readLine()) != null) {
                String[] data = line.split(" ");
                if (data[2].equalsIgnoreCase("w")) {
                    int yearsUntilRetirement = WOMAN_AGE - Integer.parseInt(data[3]);
                    outW.write(data[0] + " " + data[1] + " " + yearsUntilRetirement + "\n");
                }
                if (data[2].equalsIgnoreCase("m")) {
                    int yearsUntilRetirement = MAN_AGE - Integer.parseInt(data[3]);
                    outM.write(data[0] + " " + data[1] + " " + yearsUntilRetirement + "\n");
                }
            }
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            if (in != null) {
                in.close();
            }

            if (outW != null) {
                outW.close();
            }

            if (outM != null) {
                outM.close();
            }
        }
    }
}