package playlist;

import java.util.Arrays;

public class Main {

    public static void main(String[] args) {

        Song song1 = new Song("Hello", 12*3600);
        Song song2 = new Song("Bye", 45);
        Song song3 = new Song("What's up", 11);
        Playlist playlist = new Playlist();

        /*System.out.println(playlist.addSongLength(song1));
        System.out.println(playlist.addSongLength(song2));
        System.out.println(playlist.addSongLength(song3));*/

        int overallDuration = playlist.addSongLength(song1, song2, song3);

        System.out.println(overallDuration);
        System.out.println(playlist.printFormattedSongsDuration(song1, song2, song3));
    }
}
