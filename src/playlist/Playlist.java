package playlist;

public class Playlist {

    private int duration;

    public int addSongLength(Song... songs) {
        duration = 0;
        for (Song song : songs) {
            duration += song.getLength();
        }
        return duration;
    }

    public String timeToString(int time) {
        String timeAsStr = "";
        if (time < 10) {
            timeAsStr = "0" + String.valueOf(time);
        } else {
            timeAsStr += String.valueOf(time);
        }
        return timeAsStr;
    }


    // show in hh:mm:ss format

    public String printFormattedSongsDuration(Song... songs) {

        int hh;
        int mm;
        int ss;

        int durationInSeconds = addSongLength(songs);

        hh = durationInSeconds / 3600;
        mm = (durationInSeconds % 3600)/60;
        ss = durationInSeconds % 60;

        return timeToString(hh) + ":" + timeToString(mm) + ":" + timeToString(ss);
    }
}
