package figures;

public class Rectangle implements Figure {

    double sideA;
    double sideB;

    public Rectangle(double sideA, double sideB) {
        this.sideA = sideA;
        this.sideB = sideB;
    }

    public double getSideA() {
        return sideA;
    }

    public double getSideB() {
        return sideB;
    }

    @Override
    public double getPerimeter() {
        return 2*getSideA() + 2*getSideB();
    }

    @Override
    public double getArea() {
        return getSideA()*getSideB();
    }

    @Override
    public String getType() {
        return "Rectangle";
    }
}
