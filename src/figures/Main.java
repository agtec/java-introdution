package figures;

public class Main {
    public static void main(String[] args) {

        Function function = new Function();
        function.run();

        System.out.println("Printing from table: ");
        function.printAllFigures(function.getFigures());
    }
}
