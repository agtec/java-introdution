package figures;

public class Triangle implements Figure {

    double sideA;
    double sideB;
    double sideC;

    public Triangle(double sideA, double sideB) {
        this.sideA = sideA;
        this.sideB = sideB;
        this.sideC = Math.sqrt((Math.pow(sideA, 2) + (Math.pow(sideB, 2))));
    }

    public double getSideA() {
        return sideA;
    }

    public double getSideB() {
        return sideB;
    }

    public double getSideC() {
        return sideC;
    }

    @Override
    public double getPerimeter() {
        return getSideA() + getSideB() + getSideC();
    }

    @Override
    public double getArea() {
        return (getSideA() * getSideB())/2;
    }

    @Override
    public String getType() {
        return "Triangle";
    }
}
