package figures;

public interface Figure {

    public double getPerimeter();

    public double getArea();

    public String getType();
}
