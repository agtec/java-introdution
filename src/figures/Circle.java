package figures;

public class Circle implements Figure {

    double radius;

    public Circle(double radius) {
        this.radius = radius;
    }

    public double getRadius() {
        return radius;
    }

    @Override
    public double getPerimeter() {
        return 2*Math.PI*getRadius();
    }

    @Override
    public double getArea() {
        return 2*Math.PI*(Math.pow(getRadius(), 2));
    }

    @Override
    public String getType() {
        return "Circle";
    }
}
