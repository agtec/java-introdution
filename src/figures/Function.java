package figures;

import java.util.Scanner;

public class Function {

    private Figure[] figures = new Figure[10];
    private int index = 0;

    public Figure[] getFigures() {
        return figures;
    }

    Scanner sc = new Scanner(System.in);

    public void run() {
        System.out.println("Enter the number of figures: ");
        try {
            int numbersOfFigures = sc.nextInt();
            sc.nextLine();
            while (numbersOfFigures > 0) {
                chooseFigure();
                numbersOfFigures--;
            }
        } catch (Exception e) {
            System.out.println("Incorrect data");
        }
    }

    public void chooseFigure() {
        System.out.println("Enter the figure name: ");
        String name = sc.nextLine();
        if (name.equalsIgnoreCase("circle")) {
            Circle circle = createCircle();
            printPerimeter(circle);
            printArea(circle);
            addToFigures(circle);
        } else if (name.equalsIgnoreCase("triangle")) {
            Triangle triangle = createTriangle();
            printPerimeter(triangle);
            printArea(triangle);
            addToFigures(triangle);
        } else if (name.equalsIgnoreCase("rectangle")) {
            Rectangle rectangle = createRectangle();
            printPerimeter(rectangle);
            printArea(rectangle);
            addToFigures(rectangle);
        } else {
            System.out.println("Unknown name of the figure");
        }
    }

    private Circle createCircle() {
        System.out.println("Enter the radius: ");
        double radius = sc.nextDouble();
        sc.nextLine();
        Circle circle = new Circle(radius);
        return circle;
    }

    private Triangle createTriangle() {
        System.out.println("Enter the length of the first side of triangle: ");
        double sideA = sc.nextDouble();
        sc.nextLine();
        System.out.println("Enter the length of the second side of triangle: ");
        double sideB = sc.nextDouble();
        sc.nextLine();
        Triangle triangle = new Triangle(sideA, sideB);
        return triangle;
    }

    private Rectangle createRectangle() {
        System.out.println("Enter the length of the first side of rectangle: ");
        double sideA = sc.nextDouble();
        sc.nextLine();
        System.out.println("Enter the length of the second side of rectangle: ");
        double sideB = sc.nextDouble();
        sc.nextLine();
        Rectangle rectangle = new Rectangle(sideA, sideB);
        return rectangle;
    }

    private void printPerimeter(Figure figure) {
        System.out.printf("%s perimeter is %.2f", figure.getType(), figure.getPerimeter());
        System.out.println();
    }

    private void printArea(Figure figure) {
        System.out.printf("%s area is %.2f", figure.getType(), figure.getArea());
        System.out.println();
    }

    private void addToFigures(Figure figure) {
        if (index < figures.length) {
            figures[index] = figure;
            index++;
        } else {
            System.out.println("the maximum number of figures is 10, the number has been exceeded");
        }
    }

    public void printAllFigures(Figure[] figures) {
        for (int i = 0; i < figures.length; i++) {
            if (figures[i] != null) {
                System.out.printf("%s perimeter: %.2f, area: %.2f", figures[i].getType(), figures[i].getPerimeter(), figures[i].getArea());
                System.out.println();
            }
        }
    }
}
