package java8tasks;

import java.util.logging.Level;
import java.util.logging.Logger;

public interface MessagePrintable {

    default void printMessage() {
        System.out.println("Hello World");
        Logger logger = Logger.getLogger(LogMessage.class.getName());
        logger.log(Level.INFO, "");
    }
}
