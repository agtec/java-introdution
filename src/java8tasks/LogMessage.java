package java8tasks;

import java.util.logging.Level;
import java.util.logging.Logger;

public class LogMessage implements MessagePrintable {

    public void printMessage() {
        Logger logger = Logger.getLogger(LogMessage.class.getName());
        logger.log(Level.INFO, "Info z log message");
    }
}
