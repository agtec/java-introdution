package java8tasks;

public class Main {

    public static void main(String[] args) {

        Student student1 = new Student("C", 20, false);
        student1.printMessage();

        LogMessage logMessage = new LogMessage();
        logMessage.printMessage();

        ConsoleMessage consoleMessage = new ConsoleMessage();
        consoleMessage.printMessage();
    }
}
