package java8tasks;

import java.util.logging.Level;
import java.util.logging.Logger;

public class Student implements MessagePrintable {

    private String name;
    private Integer age;
    private Boolean isOk;
    private Logger logger = Logger.getLogger(LogMessage.class.getName());

    public Student(String name, Integer age, Boolean isOk) {
        this.name = name;
        this.age = age;
        this.isOk = isOk;
    }

    @Override
    public void printMessage() {
        logger.log(Level.INFO, "Name: " + name + ", age: " + age + ", is Ok: " + isOk);

    }
}
